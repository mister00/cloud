from flask import Flask, jsonify
import psycopg2
import time

app = Flask(__name__)

i = 0
while i < 30:
    try:
        conn = psycopg2.connect(dbname="postgres", user="postgres", password="123456", host="db")
        cur = conn.cursor()
        break
    except Exception:
        if i == 29:
            raise
        i += 1
        time.sleep(0.2)

@app.route('/ping', methods=['POST'])
def ping():
    cur.execute("CREATE TABLE IF NOT EXISTS ping (ping TEXT)")
    cur.execute("INSERT INTO ping VALUES ('ping')")
    conn.commit()
    cur.execute("SELECT COUNT(*) AS count FROM ping")
    data = cur.fetchone()
    response = {'count': data[0]}
    return jsonify(response)

@app.route('/ping/count', methods=['POST'])
def ping_count():
    cur.execute("CREATE TABLE IF NOT EXISTS ping (ping TEXT)")
    cur.execute("SELECT COUNT(*) AS count FROM ping")
    data = cur.fetchone()
    response = {'count': data[0]}
    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True, port=5001, host='0.0.0.0')
